<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;
    
    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;

        } else {
			try {
				$this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8',
				'root',
				'',
				array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)); 
			} catch (PDOException $ex) {
				echo "Connection failed: " . $ex;
			}
        }
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList()  
    {

		try {
			$booklist = array();
			$stmt = $this->db->query('SELECT id, title, author, description FROM book ORDER BY id');
		
			while($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$booklist[] = new book($row['title'], $row['author'],
										$row['description'], $row['id']);
			}
			return $booklist;
		} catch(PDOException $ex) {
			echo "Error, Could not get book list: " . $ex;
		}
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    { 
		self::verifyId($id);
		$book = null;
		$query = $this->db->prepare("SELECT * FROM book WHERE id=:id");
		$query->bindParam(":id", $id, PDO::PARAM_INT);
		$query->execute();
		
		$info = $query->fetch(PDO::FETCH_ASSOC);
				
		$book = new book($info['title'], $info['author'], $info['description'], $info['id']);
		return $book;

	}
    
    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
		self::verifyBook($book, true);
		if($book->title != '' && $book->author != '') {
			$stmt = $this->db->prepare('INSERT INTO book(title, author, description)'.
										'VALUES(:title, :author, :description)');
			$stmt->bindValue(':title', $book->title);
			$stmt->bindValue(':author', $book->author);
			$stmt->bindValue(':description', $book->description);
			$stmt->execute();
			$book->id = $this->db->lastInsertId();
			}	
    }

    /** Modifies data related to a book in the collection.
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
		self::verifyBook($book);
	
		$stmt = $this->db->prepare("UPDATE book SET title=?, author=?, description=? WHERE id=?");	
		$stmt->bindValue(1, $book->title, PDO::PARAM_STR);
		$stmt->bindValue(2, $book->author, PDO::PARAM_STR);
		$stmt->bindValue(3, $book->description, PDO::PARAM_STR);
		$stmt->bindValue(4, $book->id, PDO::PARAM_INT);
		$stmt->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
		try{
			if(is_numeric($id) != false && $id > -1){
				$book = $this->getBookById($id);
				if($book->title != ''){
					if($book->author != ''){
						$stmt = $this->db->prepare("DELETE FROM book WHERE id=?");
						$stmt->bindValue(1, $book->id, PDO::PARAM_INT);
						$stmt->execute();
					}
				}
			}
		}catch(PDOException $ex){
			echo "Error, could not update book: " . $ex;
		}
    }
}
