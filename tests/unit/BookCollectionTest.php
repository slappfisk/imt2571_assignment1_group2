<?php

require_once('src/Model/DBModel.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;
    
    protected function _before()
    {
        $db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8',
                'root',
				'',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
        $bookList = $this->dbModel->getBookList();

        // Sample tests of book list contents
        $this->assertEquals(count($bookList), 3);
        $this->assertEquals($bookList[0]->id, 1);
        $this->assertEquals($bookList[0]->title, 'Jungle Book');
        $this->assertEquals($bookList[1]->id, 2);
        $this->assertEquals($bookList[1]->author, 'J. Walker');
        $this->assertEquals($bookList[2]->id, 3);
        $this->assertEquals($bookList[2]->description, 'Written by some smart gal.'); 
    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals($book->id, 1);
        $this->assertEquals($book->title, 'Jungle Book');
        $this->assertEquals($book->author, 'R. Kipling');
        $this->assertEquals($book->description, 'A classic book.');
    }

    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected(){
		
        $this->tester->expectException(InvalidArgumentException::class, function() {
            $this->dbModel->getBookById("1'; drop table book;--");
        });
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description" 
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
	
    protected function addSomething($a, $b, $c)
    {
        $testValues = ['title' => $a,
                       'author' => $b,
                       'description' => $c];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
        
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);
        
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
		
	}
	
    public function testAddBook1()
    {
        $this->addSomething("New book","Some author","Some description");
    }
	
	public function testAddBook2()
    {
        $this->addSomething("New book","Some author","");
    }
	
	public function testAddBook3()
    {
        $this->addSomething("<script>document.body.style.visibility='hidden'</script>","<script>document.body.style.visibility='hidden'</script>","<script>document.body.style.visibility='hidden'</script>");
    }
	

    // Tests that adding a book fails if id is not numeric
  /*  public function testAddBookRejectedOnInvalidId()
    {
		try {
			$testValues = ['id' => 'm',
						   'title' => 'New book',
                           'author' => 'Some author',
                           'description' => 'Some description'];
			$book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);
			$this->dbModel->addBook($book);
			$this->assertInstanceOf(InvalidArgumentException::class, null);
		} catch (InvalidArgumentException $e) {}
		
    }*/

    // Tests that adding a book fails mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
		try {
			$testValues = ['title' => '',
							'author' => 'Some author',
							'description' => 'Some description'];
			$book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
			$this->dbModel->addBook($book);
			$this->assertInstanceOf(InvalidArgumentException::class, null);
		} catch (InvalidArgumentException $e) {}
    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    protected function testModifyBook($a, $b, $c)
    {
		$book = $this->dbModel->getBookById(1);
		$book->title = $a;
		$book->author = $b;
		$book->description = $c;
		
		$this->dbModel->modifyBook($book);
		
        $this->tester->seeInDatabase('book', ['id' => $book->id,
                                              'title' => $book->title,
                                              'author' => $book->author,
                                              'description' => $book->description]);
		
    }
    
	public function testModifyBook1(){
		$this->testModifyBook("New book", "Some author", "Some description");
	}
	
	public function testModifyBook2(){
		$this->testModifyBook("New book", "Some author", "");
	}
	
	public function testModifyBook3(){
		$this->testModifyBook("<script>document.body.style.visibility='hidden'</script>", 
								"<script>document.body.style.visibility='hidden'</script>", 
								"<script>document.body.style.visibility='hidden'</script>");
	}
	
	
    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {
		$book = $this->dbModel->getBookById(1);  //Creating default object from empty value error
		$book->title = 'New book';
		$book->author = 'Some author';
		$book->description = 'Some description';
		$book->id = "a";
			
		try{
			$this->dbModel->modifyBook($book);
			
			$this->tester->seeInDatabase('book', ['id' => $book->id,
                                              'title' => $book->title,
                                              'author' => $book->author,
                                              'description' => $book->description]);
		}catch(InvalidArgumentException $ex){
			echo "Error, could not modify book: " . $ex;
		}
    }
    
    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {
		$book = $this->dbModel->getBookById(1);
		$book->title = 'New book';
		$book->author = '';
		$book->description = 'Some description';
		
		try{
			$this->dbModel->modifyBook($book);
			
			$this->tester->seeInDatabase('book', ['id' => $book->id,  //hvis det her blir kommentert bort får jeg ikke failure
                                              'title' => $book->title,  //men det må jo være med for å sjekke
                                              'author' => $book->author,  //skal jo ikke faile fordi DBModel passer på
                                              'description' => $book->description]);  //at mandatory fields ikke mangler.
		}catch(InvalidArgumentException $ex){
			echo "Error, could not modify book: " . $ex;
		}
    }
    
    // Tests that a book record can be successfully deleted.
    public function testDeleteBook()
    {
		$book = $this->dbModel->getBookById(1);
		$this->dbModel->deleteBook($book->id);
		
		$this->tester->dontSeeInDatabase('book', ['title' => 'Jungle Book',
													'author' => 'R. Kipling',
													'description' => 'A classic book.']);
    }
    
    // Tests that deleting a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
		$book = $this->dbModel->getBookById(1);
		$this->dbModel->deleteBook($book->id);
		
		$this->tester->dontSeeInDatabase('book', ['title' => 'Jungle Book',
													'author' => 'R. Kipling',
													'description' => 'A classic book.']);
		$this->dbModel->deleteBook($book->id);
		
    }
}